import matplotlib.pyplot as plt
import sys

inputfile = sys.argv[1]
temp = []
time = []
timecount = 0

def makeT(temp,time,inputfile):
    fig = plt.figure()
    ax = fig.add_axes([0.15, 0.1, 0.8, 0.8])
    plt.plot(time, temp, label='Module temperature')
    plt.ylim(ymax=25, ymin=15)
    plt.xlabel('Time (s)')
    #plt.hlines(y=-55,xmin=0,xmax=350,linestyles='dashdot', label='-55 $^\circ$C',color='k')
    #plt.hlines(y=60,xmin=0,xmax=9500,linestyles='dashed', label='60 $^\circ$C',color='k')
    plt.legend()
    plt.savefig(inputfile+".pdf")
    #plt.show()

f = open("{}.txt".format(inputfile), "r")
for line in f:
    temp.append(float(line.rstrip('\n')))
    timecount = timecount+1
    time.append(timecount)
makeT(temp,time,inputfile)

