## 
###plot_Hits.py: 
Script used to plot Hits per Pixel image.\
Usage: python3 plot_Hits.py occupancy.json\
Where occupancy.json is the json file under noise scan data directory.

###plot_temp.py:
Script used to plot module temperature as a function of time.\
Usage: python3 plot_temp.py temp.txt\
Where temp.txt is a txt file got from Arduino output
