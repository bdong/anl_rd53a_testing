import os
from influxdb import InfluxDBClient
import time
import datetime

def read_info(LV_voltage, LV_current, HV_voltage, HV_current):
    data_list = [{
        'measurement': 'RD53A-001-RealModule',
        'tags': {'cpu': 'dorothy'},
        'fields':{
            'time': datetime.datetime.now().strftime("%H:%M:%S"),
            'LV_voltage': float(LV_voltage),
            'LV_current': float(LV_current),
            'HV_voltage': float(HV_voltage),
            'HV_current': float(HV_current)
        }
    }]
    return data_list

client = InfluxDBClient(host='localhost',port=8086)
client.switch_database('dcsDB')

while True:
    ## check with channel you are using in PS
    str_LV_voltage = subprocess.check_output("snmpget -v 2c -m WIENER-CRATE-MIB -c public 192.168.200.50 outputMeasurementTerminalVoltage.u1", shell=True)
    str_LV_current = subprocess.check_output("snmpget -v 2c -m WIENER-CRATE-MIB -c public 192.168.200.50 outputMeasurementCurrent.u1", shell=True)
    str_HV_voltage = subprocess.check_output("snmpget -v 2c -m WIENER-CRATE-MIB -c public 192.168.200.50 outputMeasurementTerminalVoltage.u301", shell=True)
    str_HV_current = subprocess.check_output("snmpget -v 2c -m WIENER-CRATE-MIB -c public 192.168.200.50 outputMeasurementCurrent.u301", shell=True)

    LV_voltage = re.findall(r"\d*\.\d+", str_LV_voltage.decode('utf-8').split('Float: ')[1])[0]
    LV_current = re.findall(r"\d*\.\d+", str_LV_current.decode('utf-8').split('Float: ')[1])[0]
    HV_voltage = re.findall(r"\d*\.\d+", str_HV_voltage.decode('utf-8').split('Float: ')[1])[0]
    HV_current = re.findall(r"\d*\.\d+", str_HV_current.decode('utf-8').split('Float: ')[1])[0]
    client.write_points(IV_info(LV_voltage, LV_current, HV_voltage, HV_current))

#    if(float(HV_voltage) > 4):
#        os.system("snmpget -v 2c -m +WIENER-CRATE-MIB -c guru 192.168.200.50 outputSwitch.u301 i 0")

