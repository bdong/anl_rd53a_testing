from influxdb import InfluxDBClient
import datetime
import time
import serial
import os
import re
import subprocess

def read_info(LV_voltage, LV_current, HV_voltage, HV_current, module_temp, air_temp, air_humid):
    data_list = [{
        'measurement': 'RD53A-001-RealModule',
        'tags': {'cpu': 'dorothy'},
        'fields':{
            'time': datetime.datetime.now().strftime("%H:%M:%S"),
            'vddd_voltage': float(LV_voltage),
            'vddd_current': float(LV_current),
            'hv_voltage': float(HV_voltage),
            'hv_current': float(HV_current),
            'temperature': float(module_temp),
            'air_temp': float(air_temp),
            'air_humid': float(air_humid)
        }
    }]
    return data_list

def IV_info(LV_voltage, LV_current, HV_voltage, HV_current):
    data_list = [{
        'measurement': 'RD53A-001-RealModule',
        'tags': {'cpu': 'dorothy'},
        'fields':{
            'time': datetime.datetime.now().strftime("%H:%M:%S"),
            'LV_voltage': float(LV_voltage),
            'LV_current': float(LV_current),
            'HV_voltage': float(HV_voltage),
            'HV_current': float(HV_current)
        }
    }]
    return data_list

client = InfluxDBClient(host='localhost',port=8086)
client.switch_database('dcsDB')
print('Please set threshold for interlock in the Arduino program!')

while True:
    ser = serial.Serial('/dev/ttyACM0',9600)
    b = ser.readline()
    string_n = b.decode()
    string = string_n.rstrip()
    print(string)

    str_LV_voltage = subprocess.check_output("snmpget -v 2c -m WIENER-CRATE-MIB -c public 192.168.200.50 outputMeasurementTerminalVoltage.u1", shell=True)
    str_LV_current = subprocess.check_output("snmpget -v 2c -m WIENER-CRATE-MIB -c public 192.168.200.50 outputMeasurementCurrent.u1", shell=True)
    str_HV_voltage = subprocess.check_output("snmpget -v 2c -m WIENER-CRATE-MIB -c public 192.168.200.50 outputMeasurementTerminalVoltage.u301", shell=True)
    str_HV_current = subprocess.check_output("snmpget -v 2c -m WIENER-CRATE-MIB -c public 192.168.200.50 outputMeasurementCurrent.u301", shell=True)

    LV_voltage = re.findall(r"\d*\.\d+", str_LV_voltage.decode('utf-8').split('Float: ')[1])[0]
    LV_current = re.findall(r"\d*\.\d+", str_LV_current.decode('utf-8').split('Float: ')[1])[0]
    HV_voltage = re.findall(r"\d*\.\d+", str_HV_voltage.decode('utf-8').split('Float: ')[1])[0]
    HV_current = re.findall(r"\d*\.\d+", str_HV_current.decode('utf-8').split('Float: ')[1])[0]


    try:
        air_temp, air_humid, module_temp = string.split()
        client.write_points(read_info(LV_voltage, LV_current, HV_voltage, HV_current, module_temp, air_temp, air_humid))
    except ValueError:
        print("Oooops! Arduino issue, not getting three numbers. Try again ...")
    time.sleep(3)

