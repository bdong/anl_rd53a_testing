#include <Wire.h>
#include "SHTSensor.h"

SHTSensor sht;

float R0 = 100000.;
float V1;
float R1, logR1;
float Vin = 5.0;
float T[0];
float c1 = 298.15, b1=3070., b2 = 3380.;
//int pinUsed[3] = {A7, A4, A5};

float gettemp(){
  V1=(5./1023.0)*analogRead(A0);
  R1 = R0 *(Vin/V1 - 1.0);
  logR1 = log(R1/R0);
  T[0] = (1.0 / (1/c1 + ((1/3380.)*logR1)))-273.15;
  return T[0];
}


void setup()
{
 Wire.begin(); // join i2c bus (address optional for master)
 Serial.begin(9600); // start serial for output
 delay(1000);

 sht.init();
}
 
void loop()
{

 float moduletemp;
 
 float Humidity;
 float Temperature;

 while(true)
 {

 delay(10);
 if(sht.readSample()){
 Temperature = sht.getTemperature();
 Humidity = sht.getHumidity();}
 moduletemp = gettemp();

 if (moduletemp<40 && moduletemp>-45){
 analogWrite(A4, 1023); // peltier PS, relay circuit
 analogWrite(A5, 0); // module PS, would shut down module if A5 voltage set to 5V
 analogWrite(A7, 0); // alarm system, alarm on if A7 voltage set to 5V
 }

 else{
 analogWrite(A4, 0);
 analogWrite(A5, 1023);
 analogWrite(A7, 1023);
 }
 
 Serial.print(Temperature);
 Serial.print("\t");
 Serial.print(Humidity);
 Serial.print("\t");
 Serial.print(moduletemp);
 Serial.print("\n");
 delay(1000);
 return;
}
}

