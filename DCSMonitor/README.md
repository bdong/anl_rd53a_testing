## Hardware used 
1. Arduino Mega 2560
2. NTC
3. SHT85 humidity/temperature sensor (install arduino-sht library in Arduino application)
4. WIENER MPOD

## Python package dependency
1. influxDB

## How to run
Monitor power supply on Grafana: python3 IV.py

Monitor module temperature and air humidity: python3 Temp_Humid.py\
Note: Temp_Humid.py works with the monitor_sht program in Arduino application. Go to Arduino application, open monitor_sht under File/Sketchbook, Verfity and Upload after. Once it's done, run: python3 Temp_Humidi.py

How to check uploaded data on Grafana:
Open broswer, go to localhost:3000

## Influxdb with Grafana
For more information on how to add influx to grafana, check [Grafana tutorial](https://grafana.com/docs/grafana/latest/datasources/influxdb/) 
