This README explains how to register RD53A module, upload scans to localDB.\
On dorothy, please use the YARR under: /home/dorothy/Yarr1p2p2/YARR/

## Register RD53A mdoule
cd /home/dorothy/Yarr1p2p2/YARR/\
./bin/dbAccessor -C -c register_component_rd53a.json -u user.json -i site.json

## Upload scans
cd /home/dorothy/Yarr1p2p2/YARR/\
./bin/scanConsole -r configs/controller/specCfg-rd53a.json -c configs/real_modules/module1/tuned/module1_setup.json -s configs/scans/rd53a/type_of_scan.json -W\

where module1_setup.json should include chips config file.\
In each chip config:\
1. set specific NAME and CHIPID
2. set OutputActiveLane to 7
3. cdrSelSerClk to 1 for 640MBP, 3 for 160MBP
4. set CmlEn 7. Can also try to set CmlInvTap to 1, and increase CmlTapBias1 if chip is not still not working.

Things can also try: change frequency from 30k to 5k
